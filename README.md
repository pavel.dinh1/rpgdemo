# Udemy course by Gamedev.tv  
## RPG Core Combat Creator: Learn Intermediate Unity C# Coding

## Build Combat for Role Playing Game (RPG) in Unity. Tutorials Cover Code Architecture & Video Game Design.
Description
## UPDATE September 2019: We have updated and improved the course using Unity 2018.3 based on feedback from our community. Enjoy! *

* This highly acclaimed course was over 200% funded on Kickstarter, and is designed for intermediate users of Unity. We recommend you take at least the first half of our Complete Unity Developer 2D or 3D, or equivalent, as a pre-requisite.

* This course is the first part in our RPG series. The second part will focus on the inventory system and will be available as a separate course.

* Role Playing Games can be complex to create, with many interacting gameplay systems. We make it easy for you, with this online course that teaches you step-by-step how to make the foundations of any good RPG combat system - from scratch.

* In this course we're focusing on the core of any great RPG, giving your players a fun combat moment in a well crafted level, based upon a solid game design, built using a robust code architecture. You will follow our project, but also make the game entirely your own.

* This is a highly realistic and comprehensive introduction to real game development. We'll be taking you through our process step-by-step, with all decisions justified and all avenues explored.

* Fancy something a little different to get started? Check out GameDev's other courses, just look for the green logo as your guide.

* We will be dealing with player movement, NavMesh navigation, AI pathfinding, save and loading systems, basic pick ups, using asset packs, debug code, making particle effects and more. We'll also give you a robust code architecture for managing more complex projects by setting up namespaces.

* You'll need a basic grasp of Unity and C# before you get start, from there we'll be teaching you more advanced programming techniques such as C# delegates, interfaces, co-routines and more.

* Building an RPG is an amazing way to level-up you game development skills, so why not join us and get started improving your game development right now?

### Who this course is for:
* This course is for intermediate Unity users who want to create their own RPG.
* Our Complete Unity Developer course provides the perfect pre-requisite.