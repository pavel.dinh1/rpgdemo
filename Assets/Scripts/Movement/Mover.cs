﻿using RPG.Core;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        private NavMeshAgent navMesh = null;
        private Animator anim = null;

        private void Start()
        {
            navMesh = GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateAnimator();
        }

        public void StartMoveAction(Vector3 destination)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination);
        }

        public void Cancel()
        {
            navMesh.isStopped = true;
        }

        public void MoveTo(Vector3 destination)
        {
            if (navMesh)
                navMesh.SetDestination(destination);
            else
                GetComponent<NavMeshAgent>().destination = destination;

            navMesh.isStopped = false;
        }

        private void UpdateAnimator()
        {
            Vector3 velocity = navMesh.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            anim.SetFloat("ForwardSpeed", speed);
        }
    }
}